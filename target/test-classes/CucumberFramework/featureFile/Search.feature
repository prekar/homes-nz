Feature: Search Feature

Scenario: To search and verify the result for Auckland
Given I open the Homes home page
When I search for "Auckland"
Then the result should go to the correct page displaying "Auckland" results

Scenario: To search and verify the result for Petone
Given I open the Homes home page
When I search for "Petone"
Then the result should go to the correct page displaying "Petone" results


Scenario: To search and verify the result for 45 Puru Crescent
Given I open the Homes home page
When I search for "45 Puru Crescent"
Then the result should go to the correct page displaying "45 Puru Crescent" results
And that 45 Puru Cres is on top of the list