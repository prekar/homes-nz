$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Search.feature");
formatter.feature({
  "line": 1,
  "name": "Search Feature",
  "description": "\r\nScenario : To search and verify the result for Auckland\r\nGiven I open the Homes home page\r\nWhen I search for \"Auckland\"\r\nThen the result should go to the correct page displaying \"Auckland\" results",
  "id": "search-feature",
  "keyword": "Feature"
});
});