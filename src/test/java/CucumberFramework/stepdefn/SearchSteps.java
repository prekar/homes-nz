package CucumberFramework.stepdefn;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class SearchSteps {
	
	public static WebDriver driver;
	
	@Given("^I open the Homes home page$")
	public void i_open_the_Homes_home_page() throws Throwable {
		
		try {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.get("https://homes.co.nz");
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("Unable to load browser: " + e.getMessage());
		}
	}
	
	
	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String arg1) throws Throwable {
		
		WebElement searchTab = driver.findElement(By.xpath("//homes-search//input"));
		searchTab.click();
		searchTab.sendKeys(arg1);
		
		WebDriverWait wait=new WebDriverWait(driver, 30);
		try {
			WebElement searchIcon = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@class, 'addressResults')]")));
		}
		catch (Exception e) {
			System.out.println("Unable to load browser: " + e.getMessage());
		} 
		
		WebElement searchbtn = driver.findElement(By.xpath("//homes-button[contains(@class, 'searchButton')]//button"));
		searchbtn.click();
		
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
	}
	

	@Then("^the result should go to the correct page displaying \"([^\"]*)\" results$")
	public void the_result_should_go_to_the_correct_page_displaying_results(String arg1) throws Throwable {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-outline']")));

		String currentURL = driver.getCurrentUrl();

		System.out.println(currentURL + " " +arg1); 
		
		if(arg1.equals("Auckland")) {
			Assert.assertEquals("https://homes.co.nz/map/auckland/auckland?searchLoc=pdb%60Fafui%60@", currentURL);
			driver.quit();
			return;
		}
		
		if(arg1.equals("Petone")) {
			Assert.assertEquals("https://homes.co.nz/map/lower-hutt/petone?searchLoc=nvrzFsclj%60@", currentURL);
			driver.quit();
			return;
		}
		    
		if(arg1.equals("45 Puru Crescent")) {
			Assert.assertEquals(currentURL, "https://homes.co.nz/map/wellington/lyall-bay/puru-crescent/45?searchLoc=rhf%7BFyb%7Bi%60@" );
			return;
		}
			
	}

	@Then("^that (\\d+) Puru Cres is on top of the list$")
	public void that_Puru_Cres_is_on_top_of_the_list(int arg1) throws Throwable {
		
		WebElement topresult = driver.findElement(By.xpath("(//homes-grid-list//a[contains(text(),  '45 Puru Cres')])[1]"));
		
		Assert.assertEquals(true, topresult.isDisplayed());
		
		System.out.println("45 Puru Cres is displayed on top of the list");
		driver.quit();
	}

}
